var gulp = require('gulp');
var cleanCss = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var minifyHtml = require('gulp-minify-html');
var imageResize = require('gulp-image-resize');

var buildFolder = './build';

gulp.task('default', ['minify-css', 'minify-js', 'minify-html', 'resize-image']);

gulp.task('minify-css', function() {
  gulp.src('./css/*.css').pipe(cleanCss()).pipe(gulp.dest(buildFolder + '/css'));
});

gulp.task('minify-js', function() {
  gulp.src('./js/*.js').pipe(uglify()).pipe(gulp.dest(buildFolder + '/js'));
});

gulp.task('minify-html', function() {
  gulp.src('*.html').pipe(minifyHtml()).pipe(gulp.dest(buildFolder));
});

gulp.task('resize-image', function() {
  gulp.src('./images/**/*.{jpg,png}').pipe(imageResize({width: 100}).pipe(gulp.dest(buildFolder + '/images')));
});
